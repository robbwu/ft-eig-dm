      SUBROUTINE DPRIMAT( MSG, MYROW, MYCOL, A, LDA, M, N )
         IMPLICIT NONE
         DOUBLE PRECISION  A( LDA, * )
         INTEGER  LDA, M, N, I, J, MYROW, MYCOL
         CHARACTER   MSG*(*)

         IF( M.LE.0 .OR. N.LE.0 .OR. M.GE.10 .OR. N.GE.10 ) RETURN
         WRITE( *, "(A,A,I4,I4)" ) MSG, "PROCESSOR", MYROW, MYCOL

         DO I=1,M
         WRITE( *, "(100G12.5)" ) ( A(I, J), J=1, N )
         END DO

      END SUBROUTINE
