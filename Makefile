#SCALAPACK=/home/pwu/pub/pkg/scalapack-2.0.2/libscalapack.a
SCALAPACK 	= /var/tmp/pwu/scalapack-2.0.2/libscalapack.a
LAPACK		= /opt/acml5.3.0/gfortran64/lib/libacml.a
PBLAS_INC=/home/pwu/pub/pkg/scalapack-2.0.2/PBLAS/SRC

FCFLAGS= -ffixed-line-length-none -cpp
CCFLAGS=
LFLAGS=
debug:CCFLAGS += -g -DDEBUG
debug:FCFLAGS += -g -fcheck=bounds -DDEBUG 
debug:LFLAGS += -g 
debug:x-ft-pdgebrd.exe xdbrd


OBJ=ft-pdgebrd.o ft-pdlabrd.o ft-pdgemv_.o ft-pdscal_.o ft-pdcopy_.o ft-PB_CInOutV.o ft-PB_Cpaxpby.o  ft-pdlarfg.o ft-pdadjv_.o ft-pdgemm_.o ft-PB_CpgemmAB.o ft-PB_CInV.o dprimat.o pdchkmat.o

x-ft-pdgebrd.exe: x-ft-pdgebrd.o $(OBJ) libtesting.a
	mpif90 $(LFLAGS) -o $@ $^ $(SCALAPACK) $(LAPACK)

xdbrd: pdbrddriver.o  libtesting.a $(OBJ)
	mpif90 $(LFLAGS) -o $@ $^ $(SCALAPACK) $(LAPACK)


%.o:%.c
	mpicc $(CCFLAGS) -c -o $@ $^ -I $(PBLAS_INC) -I $(PBLAS_INC)/PTOOLS

%.o:%.f
	mpif90 $(FCFLAGS) -c $^

%.o:%.f90
	mpif90 $(FCFLAGS) -c $^

clean:
	rm *.exe *.o
