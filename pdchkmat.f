      SUBROUTINE PDCHKMAT( M, N, A, IA, JA, DESCA )
         IMPLICIT NONE
         INTEGER IA, JA, DESCA( 9 ), M, N
         DOUBLE PRECISION  A( DESCA( 9 ), * ), S, EPS
         PARAMETER ( EPS=1.0D-3 )
         INTEGER  MYROW, NPROW, MYCOL, NPCOL, II, JJ, MP, NQ, AROW,
     $            ACOL, I, J
         INTEGER  NUMROC


         
         CALL BLACS_GRIDINFO( DESCA( 2 ), NPROW, NPCOL, MYROW, MYCOL )
         CALL INFOG2L( IA, JA, DESCA, NPROW, NPCOL, MYROW, MYCOL, 
     $                 II, JJ, AROW, ACOL ) 

         MP = NUMROC( M, DESCA( 5 ), MYROW, AROW, NPROW )
         NQ = NUMROC( N, DESCA( 6 ), MYCOL, ACOL, NPCOL )

         DO J=JJ+1, JJ+NQ-1
            S = 0
            DO I=II, II+MP-1
               S = S + A(I, J)
            END DO
            IF( ABS(S - A( II+MP, J ) ) > EPS ) THEN
               WRITE( *, * ) 'COLUMN CHKSUM FAIL: MYROW,MYCOL,J',
     $                       MYROW,MYCOL,J
            END IF
         END DO

         DO I=II, II+MP-1
            S = 0
            DO J=JJ, JJ+NQ-1
               S = S + A( I, J )
            END DO
            IF( ABS(S - A( I, JJ+NQ ) ) > EPS ) THEN
               WRITE( *, * ) 'ROW CHKSUM FAIL: MYROW,MYCOL,I',
     $                MYROW,MYCOL,I
            END IF
         END DO


               

      END SUBROUTINE 
